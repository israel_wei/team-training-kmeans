from flask import Flask, render_template, request

import Main

app = Flask(__name__)


@app.route("/kmeans_web", methods=['GET', 'POST'])
def homepage():
    if request.method == 'GET':
        return render_template("homepage.html")
    if request.method == 'POST':
        return apply_kmeans()


def apply_kmeans():
    n_clusters = request.get_json()['n_clusters']
    centers_results, samples_results = Main.run(int(n_clusters))
    return render_template(
        'images_outputs.html', centers_results=centers_results, samples_results=samples_results
    )

# return render_template(
    #     'images_outputs.html', centers_results=centers_results, samples_results=samples_results)
    # )

if __name__ == "__main__":
    app.run(host='localhost')
